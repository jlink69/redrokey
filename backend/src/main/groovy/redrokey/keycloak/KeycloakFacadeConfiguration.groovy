package redrokey.keycloak

import com.fasterxml.jackson.annotation.JsonProperty

class KeycloakFacadeConfiguration {

	@JsonProperty("client-id")
	String clientId;

	@JsonProperty("client-secret")
	String clientSecret;

	@JsonProperty("auth-server-url")
	String authServerUrl;

}
