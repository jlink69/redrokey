package redrokey.keycloak

import groovy.transform.InheritConstructors

@InheritConstructors
class KeycloakException extends RuntimeException {
}
