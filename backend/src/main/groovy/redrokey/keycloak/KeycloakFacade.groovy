package redrokey.keycloak

import groovy.transform.CompileStatic
import javax.ws.rs.NotFoundException
import javax.ws.rs.core.Response
import org.jboss.resteasy.client.jaxrs.*
import org.keycloak.admin.client.*
import org.keycloak.admin.client.resource.*
import org.keycloak.representations.idm.*

import static java.util.concurrent.TimeUnit.SECONDS

@CompileStatic
class KeycloakFacade {

	Keycloak keycloak
	private String authServerUrl
	private String adminClient
	private String adminClientSecret

	private void initClient() {
		ResteasyClient resteasyClient = new ResteasyClientBuilder()
			.connectionPoolSize(10)
			.establishConnectionTimeout(10, SECONDS)
			.socketTimeout(2, SECONDS)
			.connectionCheckoutTimeout(10, SECONDS)
			.build()

		keycloak = KeycloakBuilder.builder()
			.serverUrl(authServerUrl)
			.realm('master')
			.grantType('client_credentials')
			.clientId(adminClient)
			.clientSecret(adminClientSecret)
			.resteasyClient(resteasyClient)
			.build()
	}

	KeycloakFacade(
		String authServerUrl,
		String adminClient,
		String adminClientSecret
	) {
		this.adminClientSecret = adminClientSecret
		this.adminClient = adminClient
		this.authServerUrl = authServerUrl
		initClient()
	}


	boolean realmExists(String realmName) {
		doesResourceExist {
			keycloak.realm(realmName).toRepresentation()
		}
	}

	RealmResource getRealm(String realmName) {
		return keycloak.realm(realmName)
	}

	void deleteRealm(String realmName) {
		try {
			keycloak.realm(realmName).remove()
		} catch (NotFoundException ignore) {
			ignore.printStackTrace()
		}
	}

	RealmResource createRealm(String realmName) {
		RealmRepresentation realmRepresentation = new RealmRepresentation(
			realm: realmName,
			displayName: realmName,
			displayNameHtml: "<h3>$realmName</h3>",
			enabled: true
		)
		keycloak.realms().create(realmRepresentation)

		// When a new realm is added, we need a new access token which allows manipulating that new resource
		initClient()

		return getRealm(realmName)
	}

	boolean roleExists(String realmName, String roleName) {
		doesResourceExist {
			getRole(realmName, roleName).toRepresentation()
		}
	}

	RoleResource createRole(String realmName, String roleName, String roleDescription = roleName) {
		RoleRepresentation roleRepresentation = new RoleRepresentation(
			name: roleName,
			description: roleDescription,
		)
		getRealm(realmName).roles().create(roleRepresentation)
		return getRole(realmName, roleName)
	}

	RoleResource getRole(String realmName, String roleName) {
		getRealm(realmName).roles().get(roleName)
	}

	boolean clientExists(String realmName, String clientId) {
		doesResourceExist {
			getClient(realmName, clientId).toRepresentation()
		}
	}

	ClientResource getClient(String realmName, String clientId) {
		List<ClientRepresentation> clients = getRealm(realmName).clients().findByClientId(clientId)
		if (clients.isEmpty()) {
			throw new NotFoundException("Could not find a client with id '$clientId'")
		}
		if (clients.size() > 1) {
			throw new KeycloakException("There are multiple clients with id '$clientId'")
		}
		return getRealm(realmName).clients().get(clients[0].id)
	}

	ClientResource createClient(String realmName, String clientId) {
		ClientRepresentation clientRepresentation = new ClientRepresentation(
			clientId: clientId
		)
		return createClient(realmName, clientRepresentation)
	}

	ClientResource createClient(String realmName, ClientRepresentation newClient) {
		def response = getRealm(realmName).clients().create(newClient)
		switch (response.status) {
			case [201]:
				return getClient(realmName, newClient.clientId)
			case [409]:
				def error = response.readEntity(ErrorRepresentation)
				if (error.errorMessage.matches('Client .* already exists')) {
					throw new KeycloakException("Client with id '${newClient.clientId}' already exists.")
				}
				throw new KeycloakException("KEYCLOAK.createClient failed (${response.status}): ${error.errorMessage}")
			default:
				handleKeycloakError(response, 201, 'KeycloakApiClientService.createClient')
				return null
		}
	}

	void deleteClient(String realmName, String clientId) {
		try {
			getClient(realmName, clientId).remove()
		} catch (NotFoundException ignore) {
			ignore.printStackTrace()
		}
	}


	ProtocolMapperRepresentation getProtocolMapper(String realmName, String clientId, String mapperName) {
		def mapperRepresentation = getClient(realmName, clientId).protocolMappers.mappers.find { it.name == mapperName }
		if (null == mapperRepresentation) {
			throw new NotFoundException("Could not find a protocol mapper for '$clientId' with name '$mapperName'")
		}
		return mapperRepresentation
	}

	boolean protocolMapperExists(String realmName, String clientId, String mapperName) {
		doesResourceExist {
			getProtocolMapper(realmName, clientId, mapperName)
		}
	}

	ProtocolMapperRepresentation createProtocolMapper(String realmName, String clientId, Map mapperConfig) {
		assert mapperConfig.name
		assert mapperConfig.protocolMapper
		def mapperName = (String) mapperConfig.name
		def mapperType = (String) mapperConfig.protocolMapper
		def config = (Map) mapperConfig.config ?: [:]

		ProtocolMapperRepresentation newProtocolMapper = new ProtocolMapperRepresentation(
			name: mapperName,
			protocol: 'openid-connect',
			protocolMapper: mapperType,
			consentRequired: false,
			config: config
		)
		Response response = getClient(realmName, clientId).protocolMappers.createMapper(newProtocolMapper)
		handleKeycloakError(response, 201, 'KeycloakFacade.createProtocolMapper')
		return getProtocolMapper(realmName, clientId, mapperName)
	}

	void updateProtocolMapper(String realmName, String clientId, ProtocolMapperRepresentation protocolMapperRepresentation) {
		getClient(realmName, clientId).protocolMappers.update(protocolMapperRepresentation.id, protocolMapperRepresentation)
	}

	List<UserRepresentation> findUsers(String realmName, Map<String, Serializable> query = [:]) {
		// Implement paging?
		def allUsers = getRealm(realmName).users().list()
		if (query.role) {
			allUsers = allUsers.findAll { user ->
				String roleName = query.role as String
				List<RoleRepresentation> useRoles = getRolesForUser(realmName, user.username)
				useRoles*.name.contains(roleName)
			}
		}
		return allUsers
	}

	UserResource createUser(String realmName, String username, String password, Set<String> roles = []) {
		UserRepresentation newUser = new UserRepresentation()
		newUser.username = username
		newUser.enabled = true

		def response = getRealm(realmName).users().create(newUser)
		switch (response.status) {
			case [201]:
				setRolesForUser(realmName, username, roles)
				setUserPassword(realmName, username, password)
				return getUser(realmName, username)
			case [409]:
				def error = response.readEntity(ErrorRepresentation)
				if ('User exists with same username' == error.errorMessage) {
					throw new KeycloakException("User with username '$username' already exists.")
				}
				throw new KeycloakException("KeycloakFacade.createUser failed (${response.status}): ${error.errorMessage}")
			default:
				handleKeycloakError(response, 201, 'KeycloakFacade.createUser')
				return null
		}
	}

	void deleteUser(String realmName, String username) {
		getUser(realmName, username).remove()
	}

	void setUserPassword(String realmName, String username, String password) {
		getUser(realmName, username).resetPassword(passwordCredential(password))
	}

	private static CredentialRepresentation passwordCredential(String password) {
		CredentialRepresentation passwordCredential = new CredentialRepresentation()
		passwordCredential.type = CredentialRepresentation.PASSWORD
		passwordCredential.value = password
		passwordCredential.temporary = false
		return passwordCredential
	}

	UserResource getUser(String realmName, UUID userId) {
		return getRealm(realmName).users().get(userId.toString())
	}

	UserResource getUser(String realmName, String username) {
		List<UserRepresentation> users = getRealm(realmName).users().search(username) //
			.findAll { it.username == username.toLowerCase() }
		if (users.isEmpty()) {
			throw new NotFoundException("Could not find a user with username '$username'")
		}
		if (users.size() > 1) {
			throw new KeycloakException("There are multiple users with username '$username'")
		}
		return getRealm(realmName).users().get(users.first().id)
	}

	List<RoleRepresentation> getRolesForUser(String realmName, String username) {
		return getRoles(getUser(realmName, username))
	}

	List<RoleRepresentation> getRolesForUser(String realmName, UUID userId) {
		UserResource user = getUser(realmName, userId)
		return getRoles(user)
	}

	private List<RoleRepresentation> getRoles(UserResource user) {
		return user.roles().realmLevel().listAll()
	}

	void setRolesForUser(String realmName, String username, Set<String> roles) {
		List<RoleRepresentation> userRoles = getRolesForUser(realmName, username)
		if (roles != (userRoles*.name as Set)) {
			UserResource userResource = getUser(realmName, username)
			userResource.roles().realmLevel().remove(userRoles)
			try {
				userResource.roles().realmLevel().add(roles.collect {
					getRole(realmName, it).toRepresentation()
				})
			} catch (NotFoundException nfe) {
				nfe.printStackTrace()
			}
		}
	}

	void deleteRoleForUser(String realmName, String username, String roleName) {
		RoleRepresentation role = getRole(realmName, roleName).toRepresentation()
		getUser(realmName, username).roles().realmLevel().remove([role])
	}


	private static Response handleKeycloakError(Response response, int expectedStatus, String module) {
		if (response.status != expectedStatus) {
			if (response.hasEntity()) {
				def error = response.readEntity(ErrorRepresentation)
				throw new KeycloakException("$module failed (${response.status}): ${error.errorMessage}")
			}
			throw new KeycloakException("$module failed (${response.status})")
		}
		return response
	}

	boolean userExists(String realmName, String username) {
		doesResourceExist {
			getUser(realmName, username)
		}
	}

	private static boolean doesResourceExist(Closure getRepresentationClosure) {
		try {
			getRepresentationClosure()
			return true
		} catch (NotFoundException nfe) {
			return false
		}

	}

}
