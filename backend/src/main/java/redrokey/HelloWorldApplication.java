package redrokey;

import javax.servlet.*;
import java.util.*;

import io.dropwizard.*;
import io.dropwizard.setup.*;
import org.eclipse.jetty.servlets.*;
import org.glassfish.jersey.server.filter.*;
import org.keycloak.adapters.*;
import org.keycloak.jaxrs.*;
import redrokey.keycloak.*;

public class HelloWorldApplication extends Application<HelloWorldConfiguration> {
	public static void main(String[] args) throws Exception {
		new HelloWorldApplication().run(args);
	}

	@Override
	public String getName() {
		return "hello-world";
	}

	@Override
	public void initialize(Bootstrap<HelloWorldConfiguration> bootstrap) {
		// nothing to do yet
	}

	@Override
	public void run(HelloWorldConfiguration configuration, Environment environment) {

		KeycloakFacadeConfiguration keycloakFacadeConfiguration = configuration.getKeycloakFacadeConfiguration();

		KeycloakDeployment keycloakDeployment =
				KeycloakDeploymentBuilder.build(configuration.getKeycloakConfiguration());
		JaxrsBearerTokenFilterImpl filter = new DropwizardBearerTokenFilterImpl(keycloakDeployment);
		environment.jersey().register(filter);

		configureCors(environment);

		final HelloWorldResource resource = new HelloWorldResource(
				configuration.getTemplate(),
				configuration.getDefaultName(),
				keycloakFacadeConfiguration
		);
		environment.jersey().register(resource);

		// support annotation @RolesAllowed
		environment.jersey().register(RolesAllowedDynamicFeature.class);

	}

	private void configureCors(Environment environment) {
		final FilterRegistration.Dynamic cors =
				environment.servlets().addFilter("CORS", CrossOriginFilter.class);

		// Configure CORS parameters
		cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

		// Add URL mapping
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}

}