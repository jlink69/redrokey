package redrokey;

import javax.annotation.*;
import javax.ws.rs.*;
import javax.ws.rs.container.*;

import java.io.*;

import org.keycloak.adapters.*;
import org.keycloak.jaxrs.*;

/**
 *
 * Adapted from https://ahus1.github.io/keycloak-dropwizard-integration/tutorial.html#_dropwizard_integration_for_bearer_only_rest_services
 *
 * Allow the filter to be created from a KeycloakDeployment instance directly to make
 * it easier in a Dropwizard environment. This overrides the initialization functionality
 * in the parent class, but keeps all the filtering logic.
 * <p>
 * annotations are necessary to handle authentication before i.e. role matching.
 * Jersey doesn't look at the annotations on the interface or the parent class.
 *
 * @author Alexander Schwartz (alexander.schwartz@gmx.net)
 */
@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class DropwizardBearerTokenFilterImpl extends JaxrsBearerTokenFilterImpl {

    public DropwizardBearerTokenFilterImpl(KeycloakDeployment keycloakDeployment) {
        deploymentContext = new AdapterDeploymentContext(keycloakDeployment);
        nodesRegistrationManagement = new NodesRegistrationManagement();
    }

	@Override
	public void filter(ContainerRequestContext request) throws IOException {
    	if (request.getMethod().equals(HttpMethod.OPTIONS)) {
    		return;
		}
		super.filter(request);
	}

}
