package redrokey;

import com.fasterxml.jackson.annotation.*;
import io.dropwizard.*;
import org.keycloak.representations.adapters.config.*;
import redrokey.keycloak.*;

public class HelloWorldConfiguration extends Configuration {

	private AdapterConfig keycloakConfiguration = new AdapterConfig();

	private KeycloakFacadeConfiguration keycloakFacadeConfiguration = new KeycloakFacadeConfiguration();

	public KeycloakFacadeConfiguration getKeycloakFacadeConfiguration() {
		return keycloakFacadeConfiguration;
	}

	public void setKeycloakFacadeConfiguration(KeycloakFacadeConfiguration keycloakFacadeConfig) {
		this.keycloakFacadeConfiguration = keycloakFacadeConfig;
	}

	public AdapterConfig getKeycloakConfiguration() {
		return keycloakConfiguration;
	}

	public void setKeycloakConfiguration(AdapterConfig keycloakConfiguration) {
		this.keycloakConfiguration = keycloakConfiguration;
	}

	@JsonProperty
	public String getTemplate() {
		return "Hello, %s!";
	}

	@JsonProperty
	public String getDefaultName() {
		return "Stranger";
	}
}