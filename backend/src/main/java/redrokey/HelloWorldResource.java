package redrokey;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.representations.idm.UserRepresentation;
import redrokey.keycloak.KeycloakFacade;
import redrokey.keycloak.KeycloakFacadeConfiguration;

@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {

  private final String template;

  private final String defaultName;

  private final AtomicLong counter;

  private final KeycloakFacade keycloakFacade;

  public HelloWorldResource(
      String template,
      String defaultName,
      KeycloakFacadeConfiguration keycloakFacadeConfiguration
  ) {
    this.template = template;
    this.defaultName = defaultName;
    this.counter = new AtomicLong();
    this.keycloakFacade = new KeycloakFacade(
        keycloakFacadeConfiguration.getAuthServerUrl(),
        keycloakFacadeConfiguration.getClientId(),
        keycloakFacadeConfiguration.getClientSecret()
    );
  }

  @GET
  public Saying sayHello(@QueryParam("user") Optional<String> username, @Context SecurityContext context) {
    KeycloakPrincipal userPrincipal = (KeycloakPrincipal) context.getUserPrincipal();
    System.out.println("Authenticated principal: " + userPrincipal.getName());
    String value =
        username.filter(un -> keycloakFacade.userExists("redrokey", un))
            .map(un -> {
              UserRepresentation userRepresentation = keycloakFacade.getUser("redrokey", un).toRepresentation();
              String name = userRepresentation.getFirstName() + " " + userRepresentation.getLastName();
              return String.format(template, name);
            }).orElse(String.format("No such user: %s", username.orElse("null")));

    return new Saying(counter.incrementAndGet(), value);
  }

}