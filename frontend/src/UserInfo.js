import React, {Component} from 'react';

class UserInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            username: "",
            name: "",
            email: "",
            roles: []
        };
        // this.props.keycloak.loadUserProfile().success(userProfile => {
        //     console.log(userProfile);
        // });
        this.props.keycloak.loadUserInfo().success(userInfo => {
            this.setState({
                id: userInfo.sub,
                username: userInfo.preferred_username,
                name: userInfo.name,
                email: userInfo.email,
                roles: this.props.keycloak.tokenParsed.realm_access.roles
            })
        });
    }

    render() {
        return (
            <div className="UserInfo">
                <p>ID: {this.state.id}</p>
                <p>Username: {this.state.username}</p>
                <p>Name: {this.state.name}</p>
                <p>Email: {this.state.email}</p>
                <p>Roles: {this.state.roles.toString()}</p>
            </div>
        );
    }
}

export default UserInfo;