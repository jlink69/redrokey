# redrokey

Proof of Concept for:
- React Frontend
- Dropwizard Backend
- Keycloak as OIDC authentication provider

## Keycloak

### Starting

`docker-compose up`
will start a keycloak server and serve the admin console on
`http://localhost:8002`. Log in with `admin:admin`.

### Configure

- In realm `master` create client `redrokey-admin`
  with the following parameters:

  |Property|Value|
  |--------|-----|
  |Name|redrokey-admin|
  |Client Protocol| Open ID Connect|
  |Access Type| confidential|
  |Standard Flow Enabled| Off|
  |Service Accounts Enabled| On|
  |Service Account Roles|admin|

- Create new realm `redrokey`.


- In realm `redrokey` create client `react-client`
  with the following parameters:

  |Property|Value|
  |--------|-----|
  |Name|react-client|
  |Client Protocol| Open ID Connect|
  |Access Type| public|
  |Standard Flow Enabled| On|
  |Direct Access Grant Enabled| On|
  |Valid Redirect URIs|http://localhost:3000/*|
  |Web Origins| +|

- In realm `redrokey` create client `dropwizard-client`
  with the following parameters:

  |Property|Value|
  |--------|-----|
  |Name|dropwizard-client|
  |Client Protocol| Open ID Connect|
  |Access Type| bearer-only|

- In realm `redrokey` add the users that should be able to log in
  and reset their passwords, e.g. `user` with password `password`.

  You can also set the user's name and email and add a role.
  Those attributes can be seen in the app.

## Backend

### Configuration

In `config.yml` section `keycloakConfiguration` set:

  |Property|Value|
  |--------|-----|
  |Name|dropwizard-client|
  |realm-public-key| <public key from "redrokey" realm>|
  |credentials / secret| <secret from client "dropwizard-client">|


In `config.yml` section `keycloakFacadeConfiguration` set:

  |Property|Value|
  |--------|-----|
  |client-id|redrokey-admin|
  |credent-secret| <secret from client "redrokey-admin">|


### Starting

```bash
cd backend
./gradlew run
```

You can check if the backend works with http://localhost:9000/hello-world?name=test.
It should be rejected with a 401.


## React Application

### Starting

```bash
cd frontend
npm start
```

You can then browse to the app on http://localhost:3000/
